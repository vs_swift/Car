//
//  main.swift
//  Car
//
//  Created by Private on 12/14/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

var bmw = Car(capacity: 120,consumption: 10, location: Point(x: 0,y: 0), model:"BMW");
var renault = Car(capacity: 90, consumption: 7, location: Point(x: 0,y: 0), model: "Renault");

print("\nModel: = \(bmw.model)")
print("Location = \(bmw.location.toString())")
print("Fuel = \(bmw.fuelAmount)")
print("Consumption = \(bmw.fuelConsumption)")

print("\nModel: = \(renault.model)")
print("Location = \(renault.location.toString())")
print("Fuel = \(renault.fuelAmount)")
print("Consumption = \(renault.fuelConsumption)")

bmw.refill(fuel: 100);
renault.refill(fuel: 85);

print("\nModel: = \(bmw.model)")
print("Location = \(bmw.location.toString())")
print("Fuel = \(bmw.fuelAmount)")
print("Consumption = \(bmw.fuelConsumption)")

print("\nModel: = \(renault.model)")
print("Location = \(renault.location.toString())")
print("Fuel = \(renault.fuelAmount)")
print("Consumption = \(renault.fuelConsumption)")

bmw.drive(x: 5,y: 5);
renault.drive(x: 7,y: 7);

print("\nModel: = \(bmw.model)")
print("Location = \(bmw.location.toString())")
print("Fuel = \(bmw.fuelAmount)")
print("Consumption = \(bmw.fuelConsumption)")

print("\nModel: = \(renault.model)")
print("Location = \(renault.location.toString())")
print("Fuel = \(renault.fuelAmount)")
print("Consumption = \(renault.fuelConsumption)")
