//
//  Point.swift
//  Car
//
//  Created by Private on 12/14/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Point {
    var x: Double
    var y: Double
    
    init (x: Double, y: Double) {
    self.x = x;
    self.y = y;
    }
    
    func toString() -> String {
    return ("\(x),\(y)")
    }
    
    func distance(other: Point) -> Double {
        return hypot(x - other.x, y - other.y)
    }
}
