//
//  Car.swift
//  Car
//
//  Created by Private on 12/14/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Car {
    var fuelAmount: Double
    var fuelCapacity: Double
    var fuelConsumption: Double
    var model: String
    var location: Point
    
    init (capacity: Double, consumption: Double, location: Point, model: String) {
    self.fuelCapacity = capacity
    self.fuelConsumption = consumption
    self.location = location
    self.model = model
    self.fuelAmount = 0
    }
    
    func drive (destination: Point) {
        let distance:Double = location.distance(other:destination)
        let fuelNeeded:Double = distance * fuelConsumption
        let newFuel:Double = fuelAmount - fuelNeeded
        if ( newFuel <= 0 ) {
            print ("Out Of Fuel")
        }
        
        fuelAmount = newFuel
        location = destination
    }
    
    func drive (x: Double, y: Double) {
        drive(destination: Point(x: x,y: y))
    }
    
    func refill (fuel:Double) {
        let newFuel:Double = fuelAmount + fuel;
        if ( newFuel > fuelCapacity ) {
            print ("To Much Fuel")
        }
    fuelAmount = newFuel;
    }
}


